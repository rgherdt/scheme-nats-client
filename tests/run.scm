(import (chicken base)
        (chicken load)
        scheme
        test)

(load-relative "../nats-client.scm")

(test-begin "NATS parsing")
(test-error (parse-nats-message "MS mychannel 9 11" "Hello"))
(let ((res (parse-nats-message "msg mychan 9 11" "Hello")))
  (test "mychan" (nats-message-subject res))
  (test 9 (nats-message-sid res))
  (test "Hello" (nats-message-payload res)))

(let ((res (parse-nats-message "msg mychan 9 INBOX.34 0" "")))
  (test 9 (nats-message-sid res))
  (test "INBOX.34" (nats-message-reply-to res))
  (test "" (nats-message-payload res)))

(test-end "NATS parsing")

(test-begin "NATS publish")
(let* ((out-port (open-output-string))
       (connection (make-nats-connection #f
                                         out-port
                                         (make-hash-table)
                                         0
                                         #f)))
  (nats-publish connection "mychannel" "Hello World!" "INBOX.1")
  (test "PUB mychannel INBOX.1 12\r\nHello World!\r\n"
        (get-output-string (get-out-port connection))))

(let* ((out-port (open-output-string))
       (connection (make-nats-connection #f
                                         out-port
                                         (make-hash-table)
                                         0
                                         #f)))
  (nats-publish connection "mychannel" "Hello World!")
  (test "PUB mychannel 12\r\nHello World!\r\n"
        (get-output-string (get-out-port connection))))

(let* ((out-port (open-output-string))
       (connection (make-nats-connection #f
                                         out-port
                                         (make-hash-table)
                                         0
                                         #f)))
  (nats-subscribe connection "mychannel" values)
  (test "SUB mychannel 0\r\n"
        (get-output-string (get-out-port connection))))

(let* ((out-port (open-output-string))
       (connection (make-nats-connection #f
                                         out-port
                                         (make-hash-table)
                                         0
                                         #f)))
  (nats-subscribe connection "mychannel" values "GROUP.1")
  (test "SUB mychannel GROUP.1 0\r\n"
        (get-output-string (get-out-port connection))))

(let* ((out-port (open-output-string))
       (connection (make-nats-connection #f
                                         out-port
                                         (make-hash-table)
                                         0
                                         #f)))
  (nats-unsubscribe connection 0 10)
  (test "UNSUB 0 10\r\n"
        (get-output-string (get-out-port connection))))

(test-end "NATS publish")

(test-exit)
