(define-library (nats-client)
  (export
   ;; predicates
   nats-connection?
   ;; parameters
   nats-verbose
   ;; NATS base functions
   nats-connect
   nats-close-connection
   nats-publish
   nats-subscribe
   nats-unsubscribe)
  (include "nats-client.scm"))
