;;
;; NATS client.
;;
; Copyright 2020 Ricardo G. Herdt
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:
;
; 1. Redistributions of source code must retain the above copyright
;    notice, this list of conditions and the following disclaimer.
; 2. Redistributions in binary form must reproduce the above copyright
;    notice, this list of conditions and the following disclaimer in the
;    documentation and/or other materials provided with the distribution.
; 3. Neither the name of the author nor the names of its
;    contributors may be used to endorse or promote products derived
;    from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
; COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
; OF THE POSSIBILITY OF SUCH DAMAGE.

(import (chicken base)
        (chicken file)
        (chicken format)
        (chicken io)
        (chicken string)
        (chicken tcp)
        r7rs
        scheme
        (srfi 18)
        (srfi 69))

;;;; NATS record types

(define-record-type <nats-connection>
  (make-nats-connection in-port out-port handler-map next-sid thread)
  nats-connection?
  (in-port get-in-port)
  (out-port get-out-port)
  (handler-map get-handler-map set-handler-map!)
  (next-sid get-next-sid set-next-sid!)
  (thread get-connection-thread set-connection-thread!))

(define-record-type <nats-message>
  (make-nats-message subject sid reply-to payload)
  nats-message?
  (subject nats-message-subject)
  (sid nats-message-sid)
  (reply-to nats-message-reply-to)
  (payload nats-message-payload))

(define-record-type <nats-exception>
  (make-nats-exception msg)
  nats-exception?
  (msg nats-exception-message))

;;;; Parameters

(define nats-verbose (make-parameter #f))

;;;; NATS basis functions

(define (server-inbound-handler connection)
  (define in-port (get-in-port connection))
  (define out-port (get-out-port connection))
  (let loop ((line (read-line in-port)))
    (cond ((eof-object? line))
          ((string=? line "PING")
           (write-string "PONG\r\n" out-port))
          (else
           (dispatch connection line)))
    (loop (read-line in-port))))

;;; Establish a connection to `host`:`port` and return a <nats-connection> object.
(define (nats-connect host . more)
  (define port (optional more 4222))
  (parameterize ((tcp-read-timeout #f)
                 (tcp-write-timeout #f))
    (define-values (in out) (tcp-connect host port))
    (let ((connection (make-nats-connection in out (make-hash-table) 0 #f)))
      (set-connection-thread!
       connection
       (thread-start!
        (lambda ()
          (server-inbound-handler connection))))
      connection)))

;;; Kill connection thread and close socket ports.
(define (nats-close-connection connection)
  (thread-terminate! (get-connection-thread connection))
  (close-input-port (get-in-port connection))
  (close-output-port (get-out-port connection)))

;;; Publish a `message` to `subject`.
(define (nats-publish connection subject message . more)
  (define reply-to (optional more #f))
  (let ((query (format #f
                       "PUB ~a ~a~a\r\n~a\r\n"
                       subject
                       (if reply-to
                           (string-append reply-to " ")
                           "")
                       (string-length message)
                       message)))
    (verbose-print query)
    (write-string query (get-out-port connection))))

;;; Subscribe to `subject` and installs the callback `handler`. Return
;;; the generated subject ID (aka sid).
;;; `handler` is a function that gets two parameters: the received
;;;  payload and a reply-to subject, if available.
(define (nats-subscribe connection subject handler . more)
  (define queue-group (optional more #f))
  (define sid
    (let ((next-sid (get-next-sid connection)))
      (set-next-sid! connection (+ 1 next-sid))
      next-sid))
  (hash-table-set! (get-handler-map connection)
                   sid
                   handler)
  (let ((query (format #f
                       "SUB ~a ~a~a\r\n"
                       subject
                       (if queue-group
                           (string-append queue-group " ")
                           "")
                       sid)))
    (verbose-print query)
    (write-string query (get-out-port connection))
    sid))

;;; Unsubscribe to the subject related to `sid`. Accepts an optional
;;; argument `max-msgs` to allow waiting for the number of given
;;; messages before automatically unsubscribing.
(define (nats-unsubscribe connection sid . more)
  (define max-msgs (optional more #f))
  (let ((query (format #f
                       "UNSUB ~a~a\r\n"
                       sid
                       (if max-msgs
                           (string-append " " (number->string max-msgs))
                           ""))))
    (verbose-print query)
    (write-string query (get-out-port connection))))

(define (dispatch connection line)
  (define fields (string-split line))
  (unless (not (null? fields))
    (raise (make-nats-exception
            (string-append "Empty message: " line))))
  (define head (car fields))
  (cond ((string=? head "+OK")
         'ok)
        ((string=? head "-ERR")
         (make-nats-exception line))
        ((string=? head "INFO")
         ;; TODO parse info into a record
         (verbose-print line))
        ((string=? head "MSG")
         (let* ((payload (read-line (get-in-port connection))) ;read next line for the body
                (msg (parse-nats-message line payload))
                (sid (nats-message-sid msg))
                (handler
                 (hash-table-ref/default (get-handler-map connection)
                                         sid
                                         #f)))
           (unless handler
             (raise (make-nats-exception
                     (string-append "Missing handler to sid"
                                    (number->string sid)))))
           (handler (nats-message-payload msg)
                    (nats-message-reply-to msg))))
        (else
         (verbose-print (string-append
                         "unrecognized message: " line)))))

;;; Parse a server NATS message into a <nats-message> record. Raise
;;; a <nats-exception> if the message is ill-formed.
(define (parse-nats-message str payload)
  (let ((header-fields (string-split str)))
    (unless (>= (length header-fields) 4)
      (raise (make-nats-exception
              (string-append "ill-formed message: " str))))
    (unless (string-ci=? (car header-fields) "msg")
      (raise (make-nats-exception
              (string-append "Message doesn't begin with 'MSG': " str))))
    (let* ((subject (cadr header-fields))
           (sid (string->number (caddr header-fields)))
           (reply-to (if (= (length header-fields) 4)
                         #f
                         (list-ref header-fields 3)))
           (num-bytes (if reply-to
                          (list-ref header-fields 4)
                          (list-ref header-fields 3))))
      (make-nats-message subject sid reply-to payload))))

;;;; Utility

(define (verbose-print msg)
  (when (nats-verbose)
    (display "[INFO] ")
    (display msg)
    (newline)
    (flush-output-port)))
