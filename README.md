# scheme-nats-client

A Scheme client for the NATS protocol.

Currently only CHICKEN Scheme is supported. The code is kept close to
the R7RS standerd, so that porting it to other Scheme implementations should be
quite trivial. Most of the work would be related to the TCP part used
by nats-connect.

This is still **alpha** software, so expect it's API to change
(suggestions welcome).

## API

### [parameter] nats-verbose

If `#t`, print all exchanged messages. Defaults to `#f`.


### [procedure] (nats-connect host [port])

Establish a connection to `host` `(string)` at `port` `(number)` and return a `<nats-connection>` object.
This starts a SRFI-18 thread, so use `nats-close~connection` to terminate it properly.
`host` is a string, `port` a number.

### [procedure] (nats-close-connection connection)

Close the `<nats-connection>` object, terminating the corresponding background
thread and closing the socket ports.

### [procedure] (nats-publish connection subject message [reply-to])

Publish `message (string)` to `connection (<nats-connection>)`. Optionally also
send a `reply-to (string)` topic to the receiver.

### [procedure] (nats-subscribe connection subject handler [queue-group])

Subscribe to `subject` and install the callback `handler`. Return
the generated subject ID `(number)` (aka sid).
`handler` is a function that gets two parameters: the received
 `payload (string)` and a `reply-to (string)` subject, if available.

### [procedure] (nats-unsubscribe connection sid [max-msgs])

Unsubscribe to the subject related to `sid`. Accepts an optional
argument `max-msgs` to allow waiting for the number of given
messages before automatically unsubscribing.
